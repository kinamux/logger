﻿using System;
using System.Collections.Generic;
using Utilities.LogManager;

using System.Configuration;

namespace Logger
{
    class Logger
    {
        const string DEFAULT_LOG_FILE_PATH = "C:\\temp\\fsLog1.txt";

        static void Main(string[] args)
        {

            string logFilePath = DEFAULT_LOG_FILE_PATH;
            string tableStoreSettings = null;
            string message = null;
            bool fFileLogger = true;
            bool listLogItems = false;

            if (args.Length > 1)
            {
                // extract the arguments to find the file path to write messages and read logs from
                int argIndex = 0;

                // check and get the log file path
                if ((args[0] == "-p") && args.Length >= argIndex + 2)
                {
                    logFilePath = args[1];
                    fFileLogger = true;
                    argIndex += 2;
                }

                // check and get the log file path
                if ((args[0] == "-t") && args.Length >= argIndex + 1)
                {
                    fFileLogger = false;
                    tableStoreSettings = ConfigurationManager.ConnectionStrings["MyTestTableStore"].ConnectionString;
                    argIndex += 1;
                }

                // see if a message was provided
                if ((args[argIndex] == "-m") && (args.Length >= argIndex +2))
                {
                    message = args[argIndex + 1];
                    argIndex += 2;
                }

                // check and get the log file path
                if ((args.Length >= argIndex + 1) && (args[argIndex] == "-l"))
                {
                    listLogItems = true;
                    argIndex += 1;
                }

                if (argIndex == 0)
                {
                    // this means we were given invalid parameters. Say so.
                    Console.WriteLine("Invalid Arguments were provided.");
                    Console.WriteLine("\nProvided right set of arguments: ");
                    Console.WriteLine("\n   [-t] [-p LogFilePath] [-l] [-m message]");
                    return;
                }
            }

            ILogStore ils = null;
            if (fFileLogger)
            {
                 ils = new FileLoggerStore(logFilePath);
            } else {
                ils = new TableStoreLogger(tableStoreSettings);
            }

            LogManager lm = new LogManager(ils);

            if (null != message)
            {
                lm.AddMessage(DateTime.UtcNow, message);
            }

            if (listLogItems)
            {
                List<LogMessage> list = lm.GetMessages(DateTime.Now.AddDays(-40), DateTime.Now);

                foreach (LogMessage msg in list)
                {
                    Console.WriteLine("{0} -> {1}", msg.timestamp, msg.message);
                }
            }

            lm.Close();
            return;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Utilities.LogManager
{
    /// <summary>
    /// class FileLoggerStore: a file backed store for storing the log messages
    /// The messages are formatted using a built in formatter (simple version) if no formatter is provided.
    /// </summary>
    public class FileLoggerStore : ILogStore, IDisposable
    {
        const int LOG_FILE_BUFFER_SIZE = 8000;
        const char LOG_MESSAGE_TIME_SEPARATOR = '#';
        private static char[] LOG_MESSAGE_TIME_SEPARATOR_STRING = new char[] {LOG_MESSAGE_TIME_SEPARATOR};

        string logFilePath= null; // full path for the log file
        StreamWriter swLog = null;

        public FileLoggerStore(string filePath)
        {
            if (String.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException("Input Log File Path is empty");

            logFilePath = filePath;
            CreateAndStartFileStream();
        }

        // Read about dispose pattern
        public void Dispose()
        {
            swLog.Dispose();
            swLog = null;
        }

        // Should be an alias of dispose
        public void Close()
        {
            if (null != swLog)
            {
                swLog.Flush();
                swLog.Close();
            }
        }

        private void CreateAndStartFileStream() 
        {
            if (null != swLog) 
            {
                return; // the file is already created and a stream is maintained in memory.
            }

            // if file exists, open that in append mode
            // if a file does not exist, create a new file
            // ToDo: add thread safety
            swLog = new StreamWriter( logFilePath, true);

            return;
        }

        /// <summary>
        /// Adds the given message to the file stream (Easy operation)
        /// </summary>
        /// <param name="dtMessage"></param>
        /// <param name="message"></param>
        public void AddMessage(DateTime dtMessage, string message)
        {
            // send it to a formatter, so in the future we can handle message changes there (Ex: encryption, compression)
            // ToDo: avoid allocating and destroying string buffers in the future.
            StringBuilder sb = FormatMessage(dtMessage, message);

            lock (swLog)
            {
                // ToDo: use Async write in the future
                swLog.WriteLine( sb);
            }
        }


        private StringBuilder FormatMessage( DateTime dtMessage, string message)
        {
            StringBuilder sb = new StringBuilder(message.Length + 20); // approximate the date time formattting to take 20 bytes

            // ToDo: use a formatter helper class to format the message and timestamp and handle any escape class issues.
            sb.Append(dtMessage.ToString());
            // sb.Append(LOG_MESSAGE_TIME_SEPARATOR); // special separator for separating datestamp from message
            // sb.Append(dtMessage.ToLongTimeString());
            sb.Append(LOG_MESSAGE_TIME_SEPARATOR); // special separator for separating datestamp from message
            sb.Append(message);
            return sb;
        }

        private LogMessage ParseLogMessage( string lineRead)
        {
            // Parse the input string to extract the log message
            string[] inputItems = lineRead.Split( LOG_MESSAGE_TIME_SEPARATOR_STRING, 2);
            LogMessage lm;
            if ((inputItems != null) && (inputItems.Length != 2))
            {
                lm = new LogMessage();
                lm.timestamp = DateTime.Parse(inputItems[0]);
//                lm.timestamp = DateTime.Parse(inputItems[1]);
                lm.message = inputItems[1];
            }
            else
            {
                throw new ArgumentException("input string is NOT in a log message format", "lineRead");
            }

            return lm;
        }

        /// <summary>
        /// Get all messages in the time range specified
        /// </summary>
        /// <param name="dtStart">starting time stamp from which to fetch logs from. 
        ///     If null, then all messages from start of log stream are fetched.</param>
        /// <param name="dtEnd">ending timestamp from till which to fetch the logs for.
        ///     If null, then all messages till the recently recorded messages are fetched.
        ///     if (dtEnd < dtStart) then no messags will be fetched.</param>
        /// <returns>list of Log Messages with the timestamps attached </returns>
        public List<LogMessage> GetMessages(DateTime dtStart, DateTime dtEnd)
        {
            List<LogMessage> messages = new List<LogMessage>();

            // ToDo: handle the non-null start and end times
            if (dtStart != null)
            {
                throw new ArgumentException("non Null Start Time is not implemented yet", "dtStart");
            }

            if (dtEnd != null)
            {
                throw new ArgumentException("non null End Time is not implemented yet", "dtEnd");
            }

            // ToDo: Need another stream reader to read all messages
            StreamReader srLog = new StreamReader(logFilePath);
            string lineRead = srLog.ReadLine();

            while (lineRead != null)
            {
                LogMessage currentLogMessage = ParseLogMessage(lineRead);
                messages.Add(currentLogMessage);
                lineRead = srLog.ReadLine();
            }

            srLog.Close();

            return messages;
        }

    } // class FileLoggerStore
}
 

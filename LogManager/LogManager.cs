﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Utilities.LogManager
{
    /// <summary>
    /// class LogManager
    ///     This class provides facilities to log messages, 
    ///     including formating the messages as appropriate.
    /// All times used for recording are expected to be in the UTC time format
    /// </summary>
    public class LogManager : IDisposable
    {
        ILogStore _loggerStore = null;
        public LogManager(ILogStore ils)
        {
            _loggerStore = ils;
        }

        public void Dispose()
        {
            Close();
        }

        public void Close()
        {
            if (null != _loggerStore)
            {
                _loggerStore.Close();
                _loggerStore = null;
            }
        }
        /// <summary>
        /// Adds a message to the log stream with the current timestamp.
        /// </summary>
        /// <param name="message"></param>
        public void AddMessage( string message)
        {
            this.AddMessage( DateTime.UtcNow, message);
        }

        /// <summary>
        /// Adds a message to the logstream with the given timestamp
        /// </summary>
        /// <param name="dtStamp">Timestamp for the message; expected format is: UTC</param>
        /// <param name="message">message payload to be logged</param>
        public void AddMessage( DateTime dtStamp, string message)
        {
            Debug.Assert(_loggerStore != null);
            _loggerStore.AddMessage(DateTime.UtcNow, message);
        }

        /// <summary>
        /// List out all the messages that were logged within the provided time interval
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<LogMessage> GetMessages(DateTime dtStart, DateTime dtEnd)
        {
            Debug.Assert(_loggerStore != null);
            return _loggerStore.GetMessages(dtStart, dtEnd);
        }
    } // class LogManager
}

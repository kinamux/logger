﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.LogManager
{

    /// <summary>
    /// Simple structure to maintain the in-memory representation of Log Messages
    /// </summary>
    public struct LogMessage
    {
        public DateTime timestamp;
        public string message;
    } // struct LogMessage

    /// <summary>
    /// ILogStore : Interface contract for storing the log information
    /// </summary>
    public interface ILogStore
    {
        /// <summary>
        /// adds the specified log message as geneated at the time specified
        /// </summary>
        /// <param name="dtMessage">timestamp for the message</param>
        /// <param name="message">specific message that needs to be logged</param>
        void AddMessage(DateTime dtMessage, string message);

        /// <summary>
        /// Get all messages in the time range specified
        /// </summary>
        /// <param name="dtStart">starting time stamp from which to fetch logs from. 
        ///     If null, then all messages from start of log stream are fetched.</param>
        /// <param name="dtEnd">ending timestamp from till which to fetch the logs for.
        ///     If null, then all messages till the recently recorded messages are fetched.
        ///     if (dtEnd < dtStart) then no messags will be fetched.</param>
        /// <returns>list of Log Messages with the timestamps attached </returns>
        List<LogMessage> GetMessages(DateTime dtStart, DateTime dtEnd);


        /// <summary>
        /// Close the underlying cached data structures in use.
        /// </summary>
        void Close();
    } // interface ILogStore
}

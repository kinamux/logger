﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace Utilities.LogManager
{
    public class TableStoreLogger : ILogStore, IDisposable
    {
        string connectionString = null;

        public TableStoreLogger(string tableStoreConnectionString)
        {
//            var tableClient = CreateTableClient();

            if (String.IsNullOrWhiteSpace( tableStoreConnectionString))
                throw new ArgumentException("Input Connection String is empty");
            connectionString  = tableStoreConnectionString;
        }

        private static CloudTable GetTable(string tableConnectionString)
        {
            // ToDo: modify so that the connection string can come from the Config
            // string tableStoreSettings = ConfigurationManager.ConnectionStrings["MyTableSetting"].ConnectionString;

            var storeAccount = CloudStorageAccount.Parse( tableConnectionString);

            /*
            var credentials = new StorageCredentials( TableStoreAccountName,
                                Convert.FromBase64String( PrimaryAccessKey));
            var storeAccount = new CloudStorageAccount( credentials, true);
             */
            var tableClient = storeAccount.CreateCloudTableClient();

            CloudTable table = tableClient.GetTableReference("LogMessages");
            table.CreateIfNotExists();
            return table;
        }

        // Read about dispose pattern
        public void Dispose()
        {
            // nothing to dispose
        }

        public void Close()
        {
            // there is no need to do any closure here.
        }

        //        const string TableStoreAccountName = "camphappy";
        //      const string PrimaryAccessKey = "5XJj0OmKnwWoejEu7zYSYKUnpBCfwnq7Rt1N94oscypa0L5cFKiW9LWTMP2t94mmbP6QyFEEJZ/qvREL60TREg==";
    
        public void AddMessage(DateTime dtMessage, string message)
        {
            CloudTable ct = GetTable( this.connectionString);

            var logEntry = new DynamicTableEntity(GetPartitionKey(dtMessage), GetRowKey(dtMessage));
            logEntry.Properties.Add("Message", new EntityProperty( message));

            TableOperation oper = TableOperation.Insert(logEntry);
            ct.Execute(oper);
        }

        private string GetRowKey(DateTime dtMessage)
        {
            return dtMessage.Ticks.ToString();
        }

        private static string GetPartitionKey( DateTime dt)
        {
            return string.Format( CultureInfo.InvariantCulture, "{0}{1}{2}", dt.Year, dt.Month, dt.Day);
        }

        public List<LogMessage> GetMessages(DateTime dtStart, DateTime dtEnd)
        {
            
            string filterA = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.GreaterThanOrEqual, GetPartitionKey(dtStart));
            string filterB = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, GetRowKey(dtStart));
            string filterC = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.LessThan, GetPartitionKey(dtEnd));
            string filterD = TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThan, GetRowKey(dtEnd));

            string combinedA = TableQuery.CombineFilters(filterA, TableOperators.And, filterB);
            string combinedB = TableQuery.CombineFilters(filterC, TableOperators.And, filterD);
            string combinedAll = TableQuery.CombineFilters(combinedA, TableOperators.And, combinedB);
            TableQuery query = new TableQuery().Where( combinedAll);

            CloudTable ct = GetTable(this.connectionString);
            List<DynamicTableEntity> list = ct.ExecuteQuery(query).ToList();

            // extract the values from the log entries
            return (from item in list 
                    select new LogMessage {
                        timestamp = new DateTime( item["RowKey"].Int64Value.Value),
                        message =   item["Message"].StringValue
                    }).ToList();
        }

    }
}
